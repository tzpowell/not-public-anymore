    function toggle_hotkeys(){
        toggle_hotkeys.ok_to_use_hotkeys = !(toggle_hotkeys.ok_to_use_hotkeys);
    }

    //returns the time in seconds from either start or end group
    function get_time_from_display(groupID){
        var elm = document.getElementsByClassName(groupID);
        var minutes = elm[0].value;
        var seconds = elm[1].value;
        return 60 * Number(minutes) + Number(seconds);
    }

    //pushes the time to the display of either start or end group
    function push_time_to_display(groupID, time){
        var seconds = time % 60;        
        var minutes = (time - seconds) / 60;
        var elm = document.getElementsByClassName(groupID);
        elm[0].value = minutes;
        elm[1].value = seconds;
    }

    //sets the current time of the song
    function set_time(seconds){
        document.getElementsByTagName("video")[0].currentTime = seconds;
    }

    //sets a stop timer
    function set_stop_timer(seconds){
        window.clearTimeout(set_stop_timer.timerID);
        var delay = (seconds * 1000) / document.getElementsByTagName("video")[0].playbackRate;
        set_stop_timer.timerID = window.setTimeout(wrapper, delay);
    }
    
    function wrapper() {
        document.getElementsByTagName("video")[0].pause();
    }
   
    //starts playing the song after a delay
    function start_in(seconds) {
        window.clearTimeout(start_in.timerID);
        start_in.timerID = window.setTimeout(wrapperTwo, 1000 * seconds);
    }
    
    function wrapperTwo(){
       document.getElementsByTagName("video")[0].play();
    }

    //gets the difference between start and end in seconds
    function get_interval(){   
        var end = (parseFloat(get_time_from_display("end")));
        var start = (parseFloat(get_time_from_display("start")));
        return end - start;
    }

    //advances song to next section
    function next_interval(){
        var interval = get_interval();
        var endtime = get_time_from_display("end");
        var starttime = get_time_from_display("start");
        push_time_to_display("start", endtime);
        push_time_to_display("end", interval + endtime); 
        play();
    }

    function toggle_end(hotkey_used){
        var the_checkbox = document.getElementById("use-end-time");
        var ending_inputs = document.getElementsByClassName("interval-controls");
        if(hotkey_used){   
            the_checkbox.checked = !(the_checkbox.checked);
        }
        if(the_checkbox.checked == true){
            for(i = 0; i < ending_inputs.length; ++i){
               ending_inputs[i].removeAttribute("disabled");
            }               
        }
        else{
            for(i = 0; i < ending_inputs.length; ++i){
               ending_inputs[i].setAttribute("disabled", null);
            }  
        }
    }

    //sets up the song, then plays, only happens from front end click
    function play(){
        document.getElementsByTagName("video")[0].pause();
        //set start time based on display
        set_time(get_time_from_display("start"));        
        var wait_before_play = parseFloat(document.getElementById("wait-selector").value);    
        //set up end time if needed
        if(document.getElementById("use-end-time").checked == true){
            set_stop_timer(get_interval() + wait_before_play);
        }
        start_in(wait_before_play);   
    }

    function set_speed(){
      var vid = document.getElementsByTagName("video")[0];
      var speeder = document.getElementById("speed-selector").value;
      speeder = speeder / 100;
      vid.playbackRate = speeder;
    }



