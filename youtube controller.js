var y = document.createElement("script");
var h = document.getElementsByTagName("head")[0];
y.setAttribute("src", "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js");
h.appendChild(y);

var x = document.createElement("div");
var target = document.getElementById("watch7-user-header");
var parent = target.parentNode;
parent.insertBefore(x, target);

x.innerHTML = 
     `<div> 
           <input class="text-input start minutes" type="number" value="0" onfocus="toggle_hotkeys()" onblur="toggle_hotkeys()"/><span> :</span>
           <input class="text-input start seconds" type="number" value="0" onfocus="toggle_hotkeys()"/><span> Start Time</span>
      </div>
      <div>
           <input class="text-input end interval-controls minutes" onfocus="toggle_hotkeys()" onblur="toggle_hotkeys()" type="number" value="0" disabled/><span> :</span>
           <input class="text-input end interval-controls seconds" onfocus="toggle_hotkeys()" onblur="toggle_hotkeys()" type="number" value="0" disabled/>
           <input id="use-end-time" type="checkbox" onclick="toggle_end(false)"/><span>Use End Time</span>
      </div>
      <div id="speed group">
          <span>play at </span>
              <input type="number" class="text-input" onfocus="toggle_hotkeys()" onblur="toggle_hotkeys()" id="speed-selector" onchange="set_speed()" min="50" max="200" value="100"/>
          <span> % speed</span>
      </div>  
      <div id="wait">
          <span>wait </span>
          <input type="number" class="text-input" onfocus="toggle_hotkeys()" onblur="toggle_hotkeys()" id="wait-selector" min="0" value="0"/>
          <span> seconds before playing</span>
      </div>
      <div>
          <button id="play" onclick="play()">Play</button>
          <button id="stop" onclick="wrapper()">Stop</button>
          <button id="next-interval" class="interval-controls" onclick="next_interval()" disabled>Next Interval</button>
      <div/> 
      `;

    function toggle_hotkeys(){
        toggle_hotkeys.ok_to_use_hotkeys = !(toggle_hotkeys.ok_to_use_hotkeys);
    }

    //returns the time in seconds from either start or end group
    function get_time_from_display(groupID){
        var elm = document.getElementsByClassName(groupID);
        var minutes = elm[0].value;
        var seconds = elm[1].value;
        return 60 * Number(minutes) + Number(seconds);
    }

    //pushes the time to the display of either start or end group
    function push_time_to_display(groupID, time){
        var seconds = time % 60;        
        var minutes = (time - seconds) / 60;
        var elm = document.getElementsByClassName(groupID);
        elm[0].value = minutes;
        elm[1].value = seconds;
    }

    //sets the current time of the song
    function set_time(seconds){
        document.getElementsByTagName("video")[0].currentTime = seconds;
    }

    //sets a stop timer
    function set_stop_timer(seconds){
        window.clearTimeout(set_stop_timer.timerID);
        var delay = (seconds * 1000) / document.getElementsByTagName("video")[0].playbackRate;
        set_stop_timer.timerID = window.setTimeout(wrapper, delay);
    }
    
    function wrapper() {
        document.getElementsByTagName("video")[0].pause();
    }
   
    //starts playing the song after a delay
    function start_in(seconds) {
        window.clearTimeout(start_in.timerID);
        start_in.timerID = window.setTimeout(wrapperTwo, 1000 * seconds);
    }
    
    function wrapperTwo(){
       document.getElementsByTagName("video")[0].play();
    }

    //gets the difference between start and end in seconds
    function get_interval(){   
        var end = (parseFloat(get_time_from_display("end")));
        var start = (parseFloat(get_time_from_display("start")));
        return end - start;
    }

    //advances song to next section
    function next_interval(){
        var interval = get_interval();
        var endtime = get_time_from_display("end");
        var starttime = get_time_from_display("start");
        push_time_to_display("start", endtime);
        push_time_to_display("end", interval + endtime); 
        play();
    }

    function toggle_end(hotkey_used){
        var the_checkbox = document.getElementById("use-end-time");
        var ending_inputs = document.getElementsByClassName("interval-controls");
        if(hotkey_used){   
            the_checkbox.checked = !(the_checkbox.checked);
        }
        if(the_checkbox.checked == true){
            for(i = 0; i < ending_inputs.length; ++i){
               ending_inputs[i].removeAttribute("disabled");
            }               
        }
        else{
            for(i = 0; i < ending_inputs.length; ++i){
               ending_inputs[i].setAttribute("disabled", null);
            }  
        }
    }

    //sets up the song, then plays, only happens from front end click
    function play(){
        document.getElementsByTagName("video")[0].pause();
        //set start time based on display
        set_time(get_time_from_display("start"));        
        var wait_before_play = parseFloat(document.getElementById("wait-selector").value);    
        //set up end time if needed
        if(document.getElementById("use-end-time").checked == true){
            set_stop_timer(get_interval() + wait_before_play);
        }
        start_in(wait_before_play);   
    }

    function set_speed(){
      var vid = document.getElementsByTagName("video")[0];
      var speeder = document.getElementById("speed-selector").value;
      speeder = speeder / 100;
      vid.playbackRate = speeder;
    }
